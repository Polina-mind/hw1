const fs = require("fs").promises;

const getFiles = async function () {
  try {
    const filesList = await fs.readdir("./tmp/");
    // console.log("filesList:", filesList);
    return filesList;
  } catch (err) {
    console.error("error:", err);
  }
};

module.exports = { getFiles };
