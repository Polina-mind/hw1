const fs = require("fs").promises;

const getFile = async function (fileName) {
  try {
    const fileContent = await fs.readFile(`./tmp/${fileName}`, "utf8");
    // console.log("fileContent:", fileContent);
    return fileContent;
  } catch (err) {
    console.error("error:", err);
  }
};

module.exports = { getFile };
