const fs = require("fs").promises;

const createFile = async function (name, contentText) {
  try {
    await fs.writeFile(`./tmp/${name}`, `${contentText}`, "utf8");
    // console.log(`${name} was created`);
  } catch (err) {
    console.error("error:", err);
  }
};

module.exports = { createFile };
