const express = require("express");
const morgan = require("morgan");
const path = require("path");
const fs = require("fs");
const { createFile } = require("./js/createFile");
const { getFiles } = require("./js/getFiles");
const { getFile } = require("./js/getFile");

const extensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
const isFilenameValid = /^[\w\-. ]+$/;
const createSuccess = {
  message: "File created successfully",
};
const specifyContent = {
  message: "Please specify `content` parameter",
};
const serverError = {
  message: "Server error",
};
const clientError = {
  message: "Client error",
};

//express server
const app = express();
const port = 8080;

app.use(express.json());
app.use(morgan("combined"));

app.post("/api/files/", (req, res) => {
  const { filename, content } = req.body;
  console.log(req.body);

  if (filename) {
    if (filename.match(isFilenameValid)) {
      createFile(filename, content);

      res.status(200).send(createSuccess);
      console.log(createSuccess);
      console.log("body:", req.body);
    } else {
      res.status(400).send(specifyContent);
      console.log(specifyContent);
    }
  } else {
    res.status(400).send(specifyContent);
    console.log(specifyContent);
  }
});

app.get("/api/files", (req, res) => {
  const fileName = req.url.substr(11);
  console.log(fileName, req.url);

  if (!fileName) {
    if (req.url === "/api/files/" || req.url === "/api/files") {
      getFiles().then((list) => {
        const filesListData = {
          message: "Success",
          files: list,
        };

        res.status(200).send(filesListData);
        console.log(filesListData);
      });
    } else {
      res.status(400).send(clientError);
      console.log(clientError);
    }
  } else {
    if (!fileName.match(isFilenameValid)) {
      res.status(400).send(clientError);
      console.log(clientError);
      return;
    }

    if (extensions.includes(path.extname(fileName))) {
      fs.readFile(`./tmp/${fileName}`, "utf8", (err, data) => {
        if (err) {
          const noFile = {
            message: `No file with ${fileName} filename found`,
          };

          res.status(400).send(noFile);
          console.log(noFile);
        } else {
          const date = new Date();
          const dateISO = date.toISOString();

          const fileData = {
            message: "Success",
            filename: fileName,
            content: data,
            extension: path.extname(fileName),
            uploadedDate: dateISO,
          };

          res.status(200).send(fileData);
          console.log(fileData);
        }
      });
    } else {
      res.status(400).send(clientError);
      console.log(clientError);
    }
  }
});

app.listen(port, () => {
  console.log("App is running on port 8080");
});
